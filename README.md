# sisop-praktikum-modul-2-2023-bj-u05



## Member
- Satria Surya Prana (5025211073)
- Abiansyah Adzani Gymnastiar (5025211077)
- I Putu Arya Prawira Wiwekananda (5025211065)

## Soal 1 (dikerjakan oleh Abiansyah Adzani Gymnastiar)
Grape-kun adalah seorang penjaga hewan di kebun binatang, dia mendapatkan tugas dari atasannya untuk melakukan penjagaan pada beberapa hewan-hewan yang ada di kebun binatang sebelum melakukan penjagaan Grape-kun harus mengetahui terlebih dahulu hewan apa aja yang harus dijaga dalam drive kebun binatang tersebut terdapat folder gambar dari hewan apa saja yang harus dijaga oleh Grape-kun. Berikut merupakan link download dari drive kebun binatang tersebut : https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq 
- Grape-kun harus mendownload file tersebut untuk disimpan pada penyimpanan local komputernya. Dan untuk melakukan melihat file gambar pada folder yang telah didownload Grape-kun harus melakukan unzip pada folder tersebut.
- Setelah berhasil melakukan unzip Grape-kun melakukan pemilihan secara acak pada file gambar tersebut untuk melakukan shift penjagaan pada hewan tersebut.
- Karena Grape-kun adalah orang yang perfeksionis Grape-kun ingin membuat direktori untuk memilah file gambar tersebut. Direktori tersebut dengan nama HewanDarat, HewanAmphibi, dan HewanAir. Setelah membuat direktori tersebut Grape-kun harus melakukan filter atau pemindahan file gambar hewan sesuai dengan tempat tinggal nya.
- Setelah mengetahui hewan apa saja yang harus dijaga Grape-kun melakukan zip kepada direktori yang dia buat sebelumnya agar menghemat penyimpanan.

- Catatan : <br>
untuk melakukan zip dan unzip tidak boleh menggunakan system

### Code dan penjelasan
- Melakukan fork dengan untuk downlaod file menggunakan execv
``` C
    pid_t pid = fork();

    if (pid < 0)
    {
        exit(EXIT_FAILURE);
    }
    else if(pid == 0)
    {
        //child process
        printf("Child process with PID %d\n", getpid());

        // download zip
        char *argv[] = {"wget", "--no-check-certificate", "-O", "binatang.zip", "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", NULL};
        execv("/bin/wget", argv);
    }
```
- Lakukan fork ulang dalam parent process untuk unzip
``` C
        pid_t pid2 = fork();

        if (pid2 < 0)
        {
            perror("Error: Fork failed");
            exit(1);
        }
        else if (pid2 == 0)
        {
            // Second child process to unzip the downloaded zip folder
            printf("Second child process with PID %d\n", getpid());

            char cwd[1024];
            if (getcwd(cwd, sizeof(cwd)) != NULL)
            {
                printf("Current working directory: %s\n", cwd);
            }
            else
            {
                perror("getcwd() error");
                return 1;
            }

            char *argv2[] = {"unzip", "binatang.zip", "-d", "binatangfolder", NULL};
            execv("/bin/unzip", argv2);
        }
```
- Pemilihan file untuk shift secara acak menggunakan fungsi berikut
``` C
void random_binatang(char *basePath)
{
    srand(time(NULL));
    //printf("randomizing\n");

    struct dirent *dp;
    DIR *dir = opendir(basePath);
    int count = 0;
    while ((dp = readdir(dir)) != NULL)
    {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
            {
                count++;
                //printf("%d\n", count);
            }
    }
    rewinddir(dir); // reset the directory stream
    int randomIndex = (rand() % (count - 1)) + 1;
    //printf("random index: %d\n", randomIndex);
    count = 1;
    while ((dp = readdir(dir)) != NULL)
    {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
        {
            if (count == randomIndex)
            {
                printf("\nShift: %s\n\n", dp->d_name);
            }
            count++;
        }
    }
}

```
- Membagi hewan ke dalam jenis direktori dilakukan oleh fungsi berikut
``` C
void split_binatang(char *basePath)
{
    struct dirent *dp;
    DIR *dir = opendir(basePath);

    mkdir("darat", 0777);
    mkdir("amphibi", 0777);
    mkdir("air", 0777);    

    while ((dp = readdir(dir)) != NULL)
    {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
        {
            char *filename = dp->d_name;
            char path[100];
            sprintf(path, "%s/%s", basePath, filename);
                
            if(strstr(filename, "amphibi") != NULL)
            {
                //printf("moving %s to amphibi folder\n", dp->d_name);
                char dest_path[100];
                sprintf(dest_path, "amphibi/%s", filename);
                rename(path, dest_path);
            }
            else if(strstr(filename, "darat") != NULL)
            {
                //printf("moving %s to darat folder\n", dp->d_name);
                char dest_path[100];
                sprintf(dest_path, "darat/%s", filename);
                rename(path, dest_path);
            }
            else if(strstr(filename, "air") != NULL)
            {
                //printf("moving %s to air folder\n", dp->d_name);
                char dest_path[100];
                sprintf(dest_path, "air/%s", filename);
                rename(path, dest_path);
            }
        }
    }
    closedir(dir);
}
```
- zip keseluruhan direktori menjadi satu dilakukan dalam child process seperti berikut
``` C
            else if (pid3 == 0)
            {
                //Third child process to determine shift and
                printf("Third child process with PID %d\n", getpid());

                //Randomize the shift
                random_binatang("binatangfolder");
                //splitting the animals into folders
                split_binatang("binatangfolder");
                //deleting the downloaded zip
                char *argv3[] = {"rm", "binatang.zip", NULL};
                execv("/bin/rm", argv3);
            }
```

## Soal 2 (dikerjakan oleh I Putu Arya Prawira Wiwekananda) (REVISI)
Sucipto adalah seorang seniman terkenal yang berasal dari Indonesia. Karya nya sudah terkenal di seluruh dunia, dan lukisannya sudah dipajang di berbagai museum mancanegara. Tetapi, akhir-akhir ini sucipto sedang terkendala mengenai ide lukisan ia selanjutnya. Sebagai teman yang jago sisop, bantu sucipto untuk melukis dengan mencarikannya gambar-gambar di internet sebagai referensi !

- Pertama-tama, buatlah sebuah folder khusus, yang dalamnya terdapat sebuah program C yang per 30 detik membuat sebuah folder dengan nama timestamp [YYYY-MM-dd_HH:mm:ss].
- Tiap-tiap folder lalu diisi dengan 15 gambar yang di download dari https://picsum.photos/ , dimana tiap gambar di download setiap 5 detik. Tiap gambar berbentuk persegi dengan ukuran (t%1000)+50 piksel dimana t adalah detik Epoch Unix. Gambar tersebut diberi nama dengan format timestamp [YYYY-mm-dd_HH:mm:ss].
- Agar rapi, setelah sebuah folder telah terisi oleh 15 gambar, folder akan di zip dan folder akan di delete(sehingga hanya menyisakan .zip).
- Karena takut program tersebut lepas kendali, Sucipto ingin program tersebut men-generate sebuah program "killer" yang siap di run(executable) untuk menterminasi semua operasi program tersebut. Setelah di run, program yang menterminasi ini lalu akan mendelete dirinya sendiri.
- Buatlah program utama bisa dirun dalam dua mode, yaitu MODE_A dan MODE_B. untuk mengaktifkan MODE_A, program harus dijalankan dengan argumen -a. Untuk MODE_B, program harus dijalankan dengan argumen -b. Ketika dijalankan dalam MODE_A, program utama akan langsung menghentikan semua operasinya ketika program killer dijalankan. Untuk MODE_B, ketika program killer dijalankan, program utama akan berhenti tapi membiarkan proses di setiap folder yang masih berjalan sampai selesai(semua folder terisi gambar, terzip lalu di delete).

### Code dan penjelasan
```c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>

// Fungsi untuk membuat folder dengan nama timestamp
void create_folder(char *folder_name) {
    mkdir(folder_name, 0777);
}

// Fungsi untuk mengunduh gambar
void download_image(char *folder_name, int t, int image_num) {
    char url[50];
    sprintf(url, "https://picsum.photos/%d", (t % 1000) + 50);
    char file_name[100];
    sprintf(file_name, "%s/%s_%d.jpg", folder_name, folder_name, image_num);
    char command[100];
    sprintf(command, "wget -q %s -O %s", url, file_name);
    system(command);
}

// Fungsi untuk meng-zip folder
void zip_folder(char *folder_name) {
    char command[100];
    sprintf(command, "zip -r %s.zip %s", folder_name, folder_name);
    system(command);
}

// Fungsi untuk menghapus folder dan isiannya
void delete_folder(char *folder_name) {
    char command[100];
    sprintf(command, "rm -r %s", folder_name);
    system(command);
}

// Fungsi untuk membuat program killer
void create_killer(char *mode) {
    FILE *fp;
    char file_name[20];
    if (strcmp(mode, "-a") == 0) {
        strcpy(file_name, "killer_a.sh");
    } else if (strcmp(mode, "-b") == 0) {
        strcpy(file_name, "killer_b.sh");
    }
    fp = fopen(file_name, "w");
    fprintf(fp, "#!/bin/bash\n");
    if (strcmp(mode, "-a") == 0) {
        fprintf(fp, "killall -q program\n");
    } else if (strcmp(mode, "-b") == 0) {
        fprintf(fp, "kill -TERM $(pidof program)\n");
    }
    fprintf(fp, "rm %s\n", file_name);
    fclose(fp);
    system("chmod +x killer*.sh");
}

int main(int argc, char *argv[]) {
    int folder_count = 0;
    time_t t;
    struct tm *timestamp;
    char folder_name[20];
    int image_num = 1;
    int mode_a = 0;
    int opt;

    while ((opt = getopt(argc, argv, "ab")) != -1) {
        switch (opt) {
            case 'a':
                mode_a = 1;
                break;
            case 'b':
                mode_a = 0;
                break;
            default:
                printf("Usage: program [-a|-b]\n");
                exit(EXIT_FAILURE);
        }
    }

    create_killer(argv[1]);

    while (1) {
        // Mendapatkan timestamp sekarang
        t = time(NULL);
        timestamp = localtime(&t);
        strftime(folder_name, sizeof(folder_name), "%Y-%m-%d_%H:%M:%S", timestamp);

        // Membuat folder baru
        create_folder(folder_name);

        // Mengunduh gambar setiap 5 detik selama 15 gambar
        while (image_num <= 15) {
            download_image(folder_name, (int)t, image_num);
            image_num++;
            sleep
```

## Soal 3(Dikerjakan oleh Satria Surya Prana)
Ten Hag adalah seorang pelatih Ajax di Liga Belanda. Suatu hari, Ten Hag mendapatkan tawaran untuk menjadi manajer Manchester United. Karena Ten Hag masih mempertimbangkan tawaran tersebut, ia ingin mengenal para pemain yang akan dilatih kedepannya. Dikarenakan Ten Hag hanya mendapatkan url atau link database mentah para pemain bola, maka ia perlu melakukan klasifikasi pemain Manchester United. Bantulah Ten Hag untuk mengenal para pemain Manchester United tersebut hanya dengan 1 Program C bernama “filter.c”
- Pertama-tama, Program filter.c akan mengunduh file yang berisikan database para pemain bola. Kemudian dalam program yang sama diminta dapat melakukan extract “players.zip”. Lalu hapus file zip tersebut agar tidak memenuhi komputer Ten Hag.
- Dikarenakan database yang diunduh masih data mentah. Maka bantulah Ten Hag untuk menghapus semua pemain yang bukan dari Manchester United yang ada di directory.  
- Setelah mengetahui nama-nama pemain Manchester United, Ten Hag perlu untuk mengkategorikan pemain tersebut sesuai dengan posisi mereka dalam waktu bersamaan dengan 4 proses yang berbeda. Untuk kategori folder akan menjadi 4 yaitu Kiper, Bek, Gelandang, dan Penyerang.
- Setelah mengkategorikan anggota tim Manchester United, Ten Hag memerlukan Kesebelasan Terbaik untuk menjadi senjata utama MU berdasarkan rating terbaik dengan wajib adanya kiper, bek, gelandang, dan penyerang. (Kiper pasti satu pemain). Untuk output nya akan menjadi Formasi_[jumlah bek]-[jumlah gelandang]-[jumlah penyerang].txt dan akan ditaruh di /home/[users]/

### code dan penjelasan
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <dirent.h>
#include <string.h>
#include <fcntl.h>

void downloadzip(char url[], char namafile[]){
if (fork()==0){
   char *argv[]={"wget", "--no-check-certificate", "-O", namafile, url, NULL};
   execv("/bin/wget", argv);
   exit(EXIT_SUCCESS);
}
int status;
while(wait(&status)>=0);
return;
}

void extract(char namafile[]){
if (fork()==0){
   char *argv[]={"unzip", namafile, NULL};
   execv("/usr/bin/unzip", argv);
   exit(EXIT_SUCCESS);
}
int status;
while(wait(&status)>=0);
return;
}

void hapus(char namafile[]){
if (fork()==0){
   char *argv[]={"rm", "-r", namafile, NULL};
   execv("/usr/bin/rm", argv);
   exit(EXIT_SUCCESS);
}
int status;
while(wait(&status)>=0);
return;
}

void filterpemainMU(){
DIR *dir;
struct dirent *entry;

dir = opendir("players");
if(dir == NULL){
printf("Tidak bisa membuka directory\n");
exit(1);
}
while ((entry = readdir(dir))!= NULL){
char *filename = entry->d_name;
printf("hi");
if(strstr(filename, "ManUtd")==NULL){
char filepath[252];
printf("%s\n", filename);
sprintf(filepath, "players/%s", filename);
remove(filepath);
}
}
closedir(dir);
}

void buatdirektori(char *namadir){
char *argv[] = {"mkdir", namadir, NULL};
pid_t pid = fork();

if(pid ==0){
execvp("mkdir", argv);
perror("execvp");
exit(EXIT_FAILURE);
}
else if(pid < 0){
perror("fork");
}
else{
wait(NULL);
}
}

void kategoripemain(char *file){
    struct dirent *dp;
    DIR *dir = opendir(file);

    buatdirektori("Kiper");
    buatdirektori("Bek");
    buatdirektori("Gelandang");  
    buatdirektori("Penyerang");

    while ((dp = readdir(dir)) != NULL)
    {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
        {
            char *filename = dp->d_name;
            char path[100];
            sprintf(path, "%s/%s", file, filename);
                
            if(strstr(filename, "Kiper") != NULL)
            {
                char dest_path[100];
                sprintf(dest_path, "Kiper/%s", filename);
                int fd_sumber = open(path, O_RDONLY);
                if(fd_sumber < 0){
                    perror("Gagal membuka file sumber");
                    return;
                }

                // Buka file tujuan
                int fd_tujuan = open(dest_path, O_CREAT | O_WRONLY, 0700);
                if(fd_tujuan < 0){
                    perror("Gagal membuat file tujuan");
                    return;
                }

                // Salin isi file
                char buffer[1024];
                ssize_t nread;
                while((nread = read(fd_sumber, buffer, sizeof(buffer))) > 0){
                    write(fd_tujuan, buffer, nread);
                }

                // Tutup file
                close(fd_sumber);
                close(fd_tujuan);
            }
            else if(strstr(filename, "Bek") != NULL)
            {
                char dest_path[100];
                sprintf(dest_path, "Bek/%s", filename);
                int fd_sumber = open(path, O_RDONLY);
                if(fd_sumber < 0){
                    perror("Gagal membuka file sumber");
                    return;
                }

                // Buka file tujuan
                int fd_tujuan = open(dest_path, O_CREAT | O_WRONLY, 0700);
                if(fd_tujuan < 0){
                    perror("Gagal membuat file tujuan");
                    return;
                }

                // Salin isi file
                char buffer[1024];
                ssize_t nread;
                while((nread = read(fd_sumber, buffer, sizeof(buffer))) > 0){
                    write(fd_tujuan, buffer, nread);
                }

                // Tutup file
                close(fd_sumber);
                close(fd_tujuan);
            }
            else if(strstr(filename, "Gelandang") != NULL)
            {
                char dest_path[100];
                sprintf(dest_path, "Gelandang/%s", filename);
                int fd_sumber = open(path, O_RDONLY);
                if(fd_sumber < 0){
                    perror("Gagal membuka file sumber");
                    return;
                }

                // Buka file tujuan
                int fd_tujuan = open(dest_path, O_CREAT | O_WRONLY, 0700);
                if(fd_tujuan < 0){
                    perror("Gagal membuat file tujuan");
                    return;
                }

                // Salin isi file
                char buffer[1024];
                ssize_t nread;
                while((nread = read(fd_sumber, buffer, sizeof(buffer))) > 0){
                    write(fd_tujuan, buffer, nread);
                }

                // Tutup file
                close(fd_sumber);
                close(fd_tujuan);
            }
	    else if(strstr(filename, "Penyerang") != NULL)
            {
                char dest_path[100];
                sprintf(dest_path, "Penyerang/%s", filename);
                int fd_sumber = open(path, O_RDONLY);
                if(fd_sumber < 0){
                    perror("Gagal membuka file sumber");
                    return;
                }

                // Buka file tujuan
                int fd_tujuan = open(dest_path, O_CREAT | O_WRONLY, 0700);
                if(fd_tujuan < 0){
                    perror("Gagal membuat file tujuan");
                    return;
                }

                // Salin isi file
                char buffer[1024];
                ssize_t nread;
                while((nread = read(fd_sumber, buffer, sizeof(buffer))) > 0){
                    write(fd_tujuan, buffer, nread);
                }

                // Tutup file
                close(fd_sumber);
                close(fd_tujuan);
            }

        }
    }
    closedir(dir);
}
void buatTim(int bek, int gelandang, int penyerang){
        pid_t pid;
	int status;
	wait(&status);

	pid = fork();
	if (pid == 0){
		char command[200];
		sprintf(command,"touch Formasi_%d_%d_%d.txt && ls ./Kiper | head -n 1 >> ./Formasi_%d_%d_%d.txt", bek, gelandang, penyerang, bek, gelandang, penyerang);
		execlp("sh", "sh", "-c", command, NULL);
		exit(1);
	}
	wait(&status);

	pid = fork();
        if (pid == 0){
		char command[100];
                sprintf(command, "ls -1v ./Bek | sort -t _ -k %dn | tail -n %d >> ./Formasi_%d_%d_%d.txt", bek, bek, bek, gelandang, penyerang);
                execlp("sh", "sh", "-c", command, NULL);
                exit(1);
        }
        wait(&status);

        pid = fork();
        if (pid == 0){
                char command[100];
                sprintf(command, "ls -1v ./Gelandang | sort -t _ -k %dn | tail -n %d >> ./Formasi_%d_%d_%d.txt", gelandang, gelandang, bek, gelandang, penyerang);
                execlp("sh", "sh", "-c", command, NULL);
                exit(1);
        }
        wait(&status);

        pid = fork();
        if (pid ==0){
                char command[100];
                sprintf(command, "ls -1v ./Penyerang | sort -t _ -k %dn | tail -n %d >> ./Formasi_%d_%d_%d.txt", penyerang, penyerang, bek, gelandang, penyerang);
                execlp("sh", "sh", "-c", command, NULL);
                exit(1);
        }
        wait(&status);
}


int main(){

downloadzip("https://drive.google.com/uc?export=download&id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF", "players.zip");
extract("players.zip");
hapus("players.zip");
filterpemainMU();
kategoripemain("players");
buatTim(4,4,2);
}
//1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF



## Soal 4 (REVISI)
Banabil adalah seorang mahasiswa yang rajin kuliah dan suka belajar. Namun naasnya Banabil salah mencari teman, dia diajak ke toko mainan oleh teman-temannya dan teracuni untuk membeli banyak sekali mainan dan kebingungan memilih mainan mana yang harus dibeli. Hal tersebut menyebabkan Banabil kehilangan fokus dalam pengerjaan tugas-tugas yang diberikan oleh dosen nya. Untuk mengembalikan fokusnya, Banabil harus melatih diri sendiri dalam membuat program untuk menjalankan script bash yang menyerupai crontab dan menggunakan bahasa C karena baru dipelajari olehnya.
- Untuk menambah tantangan agar membuatnya semakin terfokus, Banabil membuat beberapa ketentuan custom yang harus dia ikuti sendiri. Ketentuan tersebut berupa:
- Banabil tidak ingin menggunakan fungsi system(), karena terlalu mudah.
- Dalam pelatihan fokus time managementnya, Banabil harus bisa membuat program yang dapat menerima argumen berupa Jam (0-23), Menit (0-59), Detik (0-59), Tanda asterisk [ * ] (value bebas), serta path file .sh.
- Dalam pelatihan fokus untuk ketepatan pilihannya, Banabil ingin programnya dapat mengeluarkan pesan “error” apabila argumen yang diterima program tidak sesuai. Pesan error dapat dibentuk sesuka hati oleh pembuat program. terserah bagaimana, yang penting tulisan error.
- Terakhir, dalam pelatihan kesempurnaan fokusnya, Banabil ingin program ini berjalan dalam background dan hanya menerima satu config cron.
- Bonus poin apabila CPU state minimum.

### code dan penjelasan
- buat daemon dengan cara berikut
``` C
  pid_t pid, sid;        // Variabel untuk menyimpan PID
  

  pid = fork();     // Menyimpan PID dari Child Process

  /* Keluar saat fork gagal
  * (nilai variabel pid < 0) */
  if (pid < 0) {
    exit(EXIT_FAILURE);
  }

  /* Keluar saat fork berhasil
  * (nilai variabel pid adalah PID dari child process) */
  if (pid > 0) {
    exit(EXIT_SUCCESS);
  }

  umask(0);

  sid = setsid();
  if (sid < 0) {
    exit(EXIT_FAILURE);
  }

  char *dir = dirname(argv[1]);
  if ((chdir(dir)) < 0) {
    exit(EXIT_FAILURE);
  }

  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);
```
- Jalankan program bash dengan dengan execvp dalam daemon
``` C
  while (1) {
    // Tulis program kalian di sini
    char *script = argv[1];
    char *args[] = {script, NULL};
    execvp(bash, args);

    sleep(30);
  }
```
### Kendala dan kesulitan pada nomor 4
- kode pada nomor 4 baru bisa menjalankan program bash di dalam daemon, namun belum bisa mengimplementasikan penjadwalan execution script bash tersebut.

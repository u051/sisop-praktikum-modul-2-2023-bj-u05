#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <libgen.h>

int main(int argc, char *argv[]) {
  pid_t pid, sid;        // Variabel untuk menyimpan PID
  

  pid = fork();     // Menyimpan PID dari Child Process

  /* Keluar saat fork gagal
  * (nilai variabel pid < 0) */
  if (pid < 0) {
    exit(EXIT_FAILURE);
  }

  /* Keluar saat fork berhasil
  * (nilai variabel pid adalah PID dari child process) */
  if (pid > 0) {
    exit(EXIT_SUCCESS);
  }

  umask(0);

  sid = setsid();
  if (sid < 0) {
    exit(EXIT_FAILURE);
  }

  char *dir = dirname(argv[1]);
  if ((chdir(dir)) < 0) {
    exit(EXIT_FAILURE);
  }

  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);
  open("/dev/null", O_RDONLY);
  open("/dev/null", O_WRONLY);
  open("/dev/null", O_WRONLY);

  while (1) {
    // Tulis program kalian di sini
    char *script = argv[1];
    char *args[] = {script, NULL};
    execvp(bash, args);

    sleep(30);
  }
}
    // while (1) {
    //     // Tulis program kalian di sini
    //     printf("gatau");
    //     pid = fork();
    //     if (pid < 0) {
    //         perror("Failed to create child process");
    //         exit(EXIT_FAILURE);
    //     }
    //     if (pid == 0) {
    //         printf("executing file %s", args[1]);
    //         execvp(args[0], &argv[5]);
    //         printf("failed to execute script");
    //         exit(EXIT_FAILURE);
    //     }
    //     waitpid(pid, NULL, 0);
    //     printf("working 3");
    //     sleep(30);
    // }
    // return 0;


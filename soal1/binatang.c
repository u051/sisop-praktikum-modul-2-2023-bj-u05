#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <time.h>
#include <dirent.h>
#include <string.h>

//https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq 

void split_binatang(char *basePath)
{
    struct dirent *dp;
    DIR *dir = opendir(basePath);

    mkdir("darat", 0777);
    mkdir("amphibi", 0777);
    mkdir("air", 0777);    

    while ((dp = readdir(dir)) != NULL)
    {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
        {
            char *filename = dp->d_name;
            char path[100];
            sprintf(path, "%s/%s", basePath, filename);
                
            if(strstr(filename, "amphibi") != NULL)
            {
                //printf("moving %s to amphibi folder\n", dp->d_name);
                char dest_path[100];
                sprintf(dest_path, "amphibi/%s", filename);
                rename(path, dest_path);
            }
            else if(strstr(filename, "darat") != NULL)
            {
                //printf("moving %s to darat folder\n", dp->d_name);
                char dest_path[100];
                sprintf(dest_path, "darat/%s", filename);
                rename(path, dest_path);
            }
            else if(strstr(filename, "air") != NULL)
            {
                //printf("moving %s to air folder\n", dp->d_name);
                char dest_path[100];
                sprintf(dest_path, "air/%s", filename);
                rename(path, dest_path);
            }
        }
    }
    closedir(dir);
}

void random_binatang(char *basePath)
{
    srand(time(NULL));
    //printf("randomizing\n");

    struct dirent *dp;
    DIR *dir = opendir(basePath);
    int count = 0;
    while ((dp = readdir(dir)) != NULL)
    {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
            {
                count++;
                //printf("%d\n", count);
            }
    }
    rewinddir(dir); // reset the directory stream
    int randomIndex = (rand() % (count - 1)) + 1;
    //printf("random index: %d\n", randomIndex);
    count = 1;
    while ((dp = readdir(dir)) != NULL)
    {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
        {
            if (count == randomIndex)
            {
                printf("\nShift: %s\n\n", dp->d_name);
            }
            count++;
        }
    }
}

int main()
{
    pid_t pid = fork();

    if (pid < 0)
    {
        exit(EXIT_FAILURE);
    }
    else if(pid == 0)
    {
        //child process
        printf("Child process with PID %d\n", getpid());

        // download zip
        char *argv[] = {"wget", "--no-check-certificate", "-O", "binatang.zip", "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", NULL};
        execv("/bin/wget", argv);
    }
    else
    {
        // Parent process
        printf("Parent process with PID %d\n", getpid());

        // Wait for the child process to finish
        int status;
        wait(&status);

        pid_t pid2 = fork();

        if (pid2 < 0)
        {
            perror("Error: Fork failed");
            exit(1);
        }
        else if (pid2 == 0)
        {
            // Second child process to unzip the downloaded zip folder
            printf("Second child process with PID %d\n", getpid());

            char cwd[1024];
            if (getcwd(cwd, sizeof(cwd)) != NULL)
            {
                printf("Current working directory: %s\n", cwd);
            }
            else
            {
                perror("getcwd() error");
                return 1;
            }

            char *argv2[] = {"unzip", "binatang.zip", "-d", "binatangfolder", NULL};
            execv("/bin/unzip", argv2);
        }
        else
        {
            printf("Parent process with PID %d\n", getpid());

            // Wait for the child process to finish
            int status;
            wait(&status);

            pid_t pid3 = fork();
            if (pid3 < 0)
            {
                perror("Error: Fork failed");
                exit(1);
            }
            else if (pid3 == 0)
            {
                //Third child process to determine shift and
                printf("Third child process with PID %d\n", getpid());

                //Randomize the shift
                random_binatang("binatangfolder");
                //splitting the animals into folders
                split_binatang("binatangfolder");
                //deleting the downloaded zip
                char *argv3[] = {"rm", "binatang.zip", NULL};
                execv("/bin/rm", argv3);
            }
            else
            {
                int status;
                wait(&status);

                //deleting an empty directory
                rmdir("binatangfolder");

                pid_t pid4 = fork();
                if (pid4 < 0)
                {
                    perror("Error: Fork failed");
                    exit(1);
                }
                else if (pid4 == 0)
                {
                    //Fourth child process to zip binatang-archive
                    //zipping
                    char *argv4[] = {"zip", "-r", "binatang-archive.zip", "air", "amphibi", "darat", NULL};
                    execv("/bin/zip", argv4);
                }
                else
                {
                    int status;
                    wait(&status);
                    
                    //deleting air, amphibi, and darat folders
                    char *argv5[] = {"rm", "-rf", "air", "amphibi", "darat", NULL};
                    execv("/bin/rm", argv5);
                }
            }
        }
    }
}
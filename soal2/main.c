#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>

// Fungsi untuk membuat folder dengan nama timestamp
void create_folder(char *folder_name) {
    mkdir(folder_name, 0777);
}

// Fungsi untuk mengunduh gambar
void download_image(char *folder_name, int t, int image_num) {
    char url[50];
    sprintf(url, "https://picsum.photos/%d", (t % 1000) + 50);
    char file_name[100];
    sprintf(file_name, "%s/%s_%d.jpg", folder_name, folder_name, image_num);
    char command[100];
    sprintf(command, "wget -q %s -O %s", url, file_name);
    system(command);
}

// Fungsi untuk meng-zip folder
void zip_folder(char *folder_name) {
    char command[100];
    sprintf(command, "zip -r %s.zip %s", folder_name, folder_name);
    system(command);
}

// Fungsi untuk menghapus folder dan isiannya
void delete_folder(char *folder_name) {
    char command[100];
    sprintf(command, "rm -r %s", folder_name);
    system(command);
}

// Fungsi untuk membuat program killer
void create_killer(char *mode) {
    FILE *fp;
    char file_name[20];
    if (strcmp(mode, "-a") == 0) {
        strcpy(file_name, "killer_a.sh");
    } else if (strcmp(mode, "-b") == 0) {
        strcpy(file_name, "killer_b.sh");
    }
    fp = fopen(file_name, "w");
    fprintf(fp, "#!/bin/bash\n");
    if (strcmp(mode, "-a") == 0) {
        fprintf(fp, "killall -q program\n");
    } else if (strcmp(mode, "-b") == 0) {
        fprintf(fp, "kill -TERM $(pidof program)\n");
    }
    fprintf(fp, "rm %s\n", file_name);
    fclose(fp);
    system("chmod +x killer*.sh");
}

int main(int argc, char *argv[]) {
    int folder_count = 0;
    time_t t;
    struct tm *timestamp;
    char folder_name[20];
    int image_num = 1;
    int mode_a = 0;
    int opt;

    while ((opt = getopt(argc, argv, "ab")) != -1) {
        switch (opt) {
            case 'a':
                mode_a = 1;
                break;
            case 'b':
                mode_a = 0;
                break;
            default:
                printf("Usage: program [-a|-b]\n");
                exit(EXIT_FAILURE);
        }
    }

    create_killer(argv[1]);

    while (1) {
        // Mendapatkan timestamp sekarang
        t = time(NULL);
        timestamp = localtime(&t);
        strftime(folder_name, sizeof(folder_name), "%Y-%m-%d_%H:%M:%S", timestamp);

        // Membuat folder baru
        create_folder(folder_name);

        // Mengunduh gambar setiap 5 detik selama 15 gambar
        while (image_num <= 15) {
            download_image(folder_name, (int)t, image_num);
            image_num++;
            sleep

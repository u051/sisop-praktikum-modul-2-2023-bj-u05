#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <dirent.h>
#include <string.h>
#include <fcntl.h>

void downloadzip(char url[], char namafile[]){
if (fork()==0){
   char *argv[]={"wget", "--no-check-certificate", "-O", namafile, url, NULL};
   execv("/bin/wget", argv);
   exit(EXIT_SUCCESS);
}
int status;
while(wait(&status)>=0);
return;
}

void extract(char namafile[]){
if (fork()==0){
   char *argv[]={"unzip", namafile, NULL};
   execv("/usr/bin/unzip", argv);
   exit(EXIT_SUCCESS);
}
int status;
while(wait(&status)>=0);
return;
}

void hapus(char namafile[]){
if (fork()==0){
   char *argv[]={"rm", "-r", namafile, NULL};
   execv("/usr/bin/rm", argv);
   exit(EXIT_SUCCESS);
}
int status;
while(wait(&status)>=0);
return;
}

void filterpemainMU(){
DIR *dir;
struct dirent *entry;

dir = opendir("players");
if(dir == NULL){
printf("Tidak bisa membuka directory\n");
exit(1);
}
while ((entry = readdir(dir))!= NULL){
char *filename = entry->d_name;
printf("hi");
if(strstr(filename, "ManUtd")==NULL){
char filepath[252];
printf("%s\n", filename);
sprintf(filepath, "players/%s", filename);
remove(filepath);
}
}
closedir(dir);
}

void buatdirektori(char *namadir){
char *argv[] = {"mkdir", namadir, NULL};
pid_t pid = fork();

if(pid ==0){
execvp("mkdir", argv);
perror("execvp");
exit(EXIT_FAILURE);
}
else if(pid < 0){
perror("fork");
}
else{
wait(NULL);
}
}

void kategoripemain(char *file){
    struct dirent *dp;
    DIR *dir = opendir(file);

    buatdirektori("Kiper");
    buatdirektori("Bek");
    buatdirektori("Gelandang");  
    buatdirektori("Penyerang");

    while ((dp = readdir(dir)) != NULL)
    {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
        {
            char *filename = dp->d_name;
            char path[100];
            sprintf(path, "%s/%s", file, filename);
                
            if(strstr(filename, "Kiper") != NULL)
            {
                char dest_path[100];
                sprintf(dest_path, "Kiper/%s", filename);
                int fd_sumber = open(path, O_RDONLY);
                if(fd_sumber < 0){
                    perror("Gagal membuka file sumber");
                    return;
                }

                // Buka file tujuan
                int fd_tujuan = open(dest_path, O_CREAT | O_WRONLY, 0700);
                if(fd_tujuan < 0){
                    perror("Gagal membuat file tujuan");
                    return;
                }

                // Salin isi file
                char buffer[1024];
                ssize_t nread;
                while((nread = read(fd_sumber, buffer, sizeof(buffer))) > 0){
                    write(fd_tujuan, buffer, nread);
                }

                // Tutup file
                close(fd_sumber);
                close(fd_tujuan);
            }
            else if(strstr(filename, "Bek") != NULL)
            {
                char dest_path[100];
                sprintf(dest_path, "Bek/%s", filename);
                int fd_sumber = open(path, O_RDONLY);
                if(fd_sumber < 0){
                    perror("Gagal membuka file sumber");
                    return;
                }

                // Buka file tujuan
                int fd_tujuan = open(dest_path, O_CREAT | O_WRONLY, 0700);
                if(fd_tujuan < 0){
                    perror("Gagal membuat file tujuan");
                    return;
                }

                // Salin isi file
                char buffer[1024];
                ssize_t nread;
                while((nread = read(fd_sumber, buffer, sizeof(buffer))) > 0){
                    write(fd_tujuan, buffer, nread);
                }

                // Tutup file
                close(fd_sumber);
                close(fd_tujuan);
            }
            else if(strstr(filename, "Gelandang") != NULL)
            {
                char dest_path[100];
                sprintf(dest_path, "Gelandang/%s", filename);
                int fd_sumber = open(path, O_RDONLY);
                if(fd_sumber < 0){
                    perror("Gagal membuka file sumber");
                    return;
                }

                // Buka file tujuan
                int fd_tujuan = open(dest_path, O_CREAT | O_WRONLY, 0700);
                if(fd_tujuan < 0){
                    perror("Gagal membuat file tujuan");
                    return;
                }

                // Salin isi file
                char buffer[1024];
                ssize_t nread;
                while((nread = read(fd_sumber, buffer, sizeof(buffer))) > 0){
                    write(fd_tujuan, buffer, nread);
                }

                // Tutup file
                close(fd_sumber);
                close(fd_tujuan);
            }
	    else if(strstr(filename, "Penyerang") != NULL)
            {
                char dest_path[100];
                sprintf(dest_path, "Penyerang/%s", filename);
                int fd_sumber = open(path, O_RDONLY);
                if(fd_sumber < 0){
                    perror("Gagal membuka file sumber");
                    return;
                }

                // Buka file tujuan
                int fd_tujuan = open(dest_path, O_CREAT | O_WRONLY, 0700);
                if(fd_tujuan < 0){
                    perror("Gagal membuat file tujuan");
                    return;
                }

                // Salin isi file
                char buffer[1024];
                ssize_t nread;
                while((nread = read(fd_sumber, buffer, sizeof(buffer))) > 0){
                    write(fd_tujuan, buffer, nread);
                }

                // Tutup file
                close(fd_sumber);
                close(fd_tujuan);
            }

        }
    }
    closedir(dir);
}
void buatTim(int bek, int gelandang, int penyerang){
        pid_t pid;
	int status;
	wait(&status);

	pid = fork();
	if (pid == 0){
		char command[200];
		sprintf(command,"touch Formasi_%d_%d_%d.txt && ls ./Kiper | head -n 1 >> ./Formasi_%d_%d_%d.txt", bek, gelandang, penyerang, bek, gelandang, penyerang);
		execlp("sh", "sh", "-c", command, NULL);
		exit(1);
	}
	wait(&status);

	pid = fork();
        if (pid == 0){
		char command[100];
                sprintf(command, "ls -1v ./Bek | sort -t _ -k %dn | tail -n %d >> ./Formasi_%d_%d_%d.txt", bek, bek, bek, gelandang, penyerang);
                execlp("sh", "sh", "-c", command, NULL);
                exit(1);
        }
        wait(&status);

        pid = fork();
        if (pid == 0){
                char command[100];
                sprintf(command, "ls -1v ./Gelandang | sort -t _ -k %dn | tail -n %d >> ./Formasi_%d_%d_%d.txt", gelandang, gelandang, bek, gelandang, penyerang);
                execlp("sh", "sh", "-c", command, NULL);
                exit(1);
        }
        wait(&status);

        pid = fork();
        if (pid ==0){
                char command[100];
                sprintf(command, "ls -1v ./Penyerang | sort -t _ -k %dn | tail -n %d >> ./Formasi_%d_%d_%d.txt", penyerang, penyerang, bek, gelandang, penyerang);
                execlp("sh", "sh", "-c", command, NULL);
                exit(1);
        }
        wait(&status);
}


int main(){

downloadzip("https://drive.google.com/uc?export=download&id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF", "players.zip");
extract("players.zip");
hapus("players.zip");
filterpemainMU();
kategoripemain("players");
buatTim(4,4,2);
}
//1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF
